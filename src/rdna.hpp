#include <string>
#include <random>

using std::string;

string randDNA(int seed, std::string bases, int n)
{
int m; 						  //used to seed the random number generator

std::string ATCG = "";		 //string containing letter bases 

int min = 0; 				//minimum length 
int max = bases.size()-1;  //maximum length 


std::mt19937 eng(seed);
std::uniform_int_distribution <int> uniform(min, max);
{
	for(m = 0; m < n; m++)
	ATCG += bases[uniform(eng)];

}

return ATCG;
}
